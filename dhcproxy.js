var dhcpd = require('./lib/dhcpd');
var ip = require("ip");


console.dir ( ip.address() );

// var fs = require("fs");
// var tftp = require("tftp");
//
// var server = tftp.createServer ({
//   host: ip.address(),
//   port: 69,
//   root: "./tftpboot",
//   denyPUT: true
// });

var server = new dhcpd({
	subnet: '0.0.0.0/0',
	// your server running this dhcproxy
	host: ip.address(),
	// your TFTP server
	tftpserver: '51.255.195.63',
	// TFTP boot filenames
	bios_filename: 'ipxe/undionly.kpxe',
	efi32_filename: 'ipxe32.efi',
	efi64_filename: 'ipxe.efi'
});
